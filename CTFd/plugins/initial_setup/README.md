# Initial Setup Plugin

Welcome, this documentation file will explain the initial setup plugin both briefly and thoroughly. The initial setup plugin is a plugin that automatically executes the CTFd initial setup process. This means that when a new CTFd environment is deployed, you no longer have to manually setup things like: event name, admin credentials, mail server, etc). Before the CTFd environment is deployed, you simply need to have configured some ENV variables. Multiple technologies are used to make this plugin work, as listed in the `Used Technologies/Concepts` section.

## 1. Features

- Automatic CTFd creation
- Custom index page creation
- Custom email configuration
- Control through ENV variables

## 2. Happy Flow

> - Plugin initializes itself
> - Event name is set
> - Event mode is set
> - Button visibilities are set
> - Index page is created
> - Index page is added to the database
> - Email system is configured
> - Notify CTFd that initial setup is done

## 3. Technical Documentation

- [ENV Variables](readme/ENV_VARIABLES.md)
- [Plugin Structure](readme/PLUGIN_STRUCTURE.md)

## 4. Abbreviations

- CTF = Capture The Flag
- HTML = HyperText Markup Language
- CSS = Cascading Style Sheets
- ENV = Environment
- SMTP = Simple Mail Transfer Protocol
- TLS = Transport Layer Security
- SSL = Secure Sockets Layer

## 5. Used Technologies/Concepts

- [CTFd](https://ctfd.io/)
- [Python](https://en.wikipedia.org/wiki/Python_(programming_language))
- [HTML](https://en.wikipedia.org/wiki/HTML)
- [CSS](https://en.wikipedia.org/wiki/CSS)
- [Happy Flow](https://en.wikipedia.org/wiki/Happy_path)
- [Environment Variable](https://en.wikipedia.org/wiki/Environment_variable)
- [SMTP](https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol)
- [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security)
- [SSL](https://en.wikipedia.org/wiki/Transport_Layer_Security)
