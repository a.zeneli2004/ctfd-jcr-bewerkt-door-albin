import base64
import flask
import itsdangerous
import CTFd

def handle(data=None):
    # Handle POST request
    if flask.request.method == "POST":
        CTFd.models.db.session.close()
        return flask.redirect("/")
    # Check if email system is disabled
    if not CTFd.utils.config.can_send_mail():
        CTFd.models.db.session.close()
        return flask.redirect("/")
    # Validate data
    if not data:
        CTFd.models.db.session.close()
        return flask.redirect("/")
    try:
        user_email = CTFd.utils.security.signing.unserialize(data, max_age=1800)
    except (itsdangerous.exc.BadTimeSignature, itsdangerous.exc.SignatureExpired, itsdangerous.exc.BadSignature, base64.binascii.Error, TypeError):
        CTFd.models.db.session.close()
        return flask.redirect("/")
    # Retrieve CTFd account
    account = CTFd.models.Users.query.filter_by(email=user_email).first()
    # Validate account
    if not account or account.verified:
        CTFd.models.db.session.close()
        return flask.redirect("/")
    # Verify account
    account.verified = True
    CTFd.models.db.session.commit()
    # Send email
    CTFd.utils.email.successful_registration_notification(account.email)
    # Go to index page
    CTFd.models.db.session.close()
    return flask.redirect("/")