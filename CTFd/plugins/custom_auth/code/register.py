import flask
import CTFd

from . import tools

def handle():
    # Temporary values
    REGISTER_PAGE = "register_page.html"
    infos = CTFd.utils.helpers.get_infos()
    errors = CTFd.utils.helpers.get_errors()
    # Handle GET request
    if flask.request.method == "GET":
        CTFd.models.db.session.close()
        return flask.render_template(REGISTER_PAGE)
    # Retrieve form data
    name = flask.request.form.get("name", "").strip()
    email_address = flask.request.form.get("email", "").strip().lower()
    password = flask.request.form.get("password", "").strip()
    # Validate form data
    if len(name) < 3:
        errors.append("Name must be at least 3 characters!")
    if len(name) > 16:
        errors.append("Name must be less than 16 characters!")
    if any(c in ",<.>/?;:'\"[{]}-_=+`~!@#$%^&*()" for c in name):
        errors.append("Name cannot contain any special characters!")
    if CTFd.models.Users.query.add_columns("name", "id").filter_by(name=name).first():
        errors.append("Name already in use!")
    if CTFd.models.Users.query.add_columns("email", "id").filter_by(email=email_address).first():
        errors.append("Email already in use!")
    errors.extend(tools.validate_strong_password(password))
    # Check for errors
    if len(errors) > 0:
        CTFd.models.db.session.close()
        return flask.render_template(REGISTER_PAGE, infos=infos, errors=errors)
    # Create new CTFd account
    if email_address == CTFd.utils.get_app_config("SETUP_EVENT_ADMIN"):
        account = CTFd.models.Admins(
        name=name,
        email=email_address,
        password=password,
        type="admin",
        website="https://ctfd.io/",
        affiliation="Unknown",
        country="NL",
        verified=not CTFd.utils.config.can_send_mail())
    else:
        account = CTFd.models.Users(
        name=name,
        email=email_address,
        password=password,
        website="https://ctfd.io/",
        affiliation="Unknown",
        country="NL",
        verified=not CTFd.utils.config.can_send_mail())
    # Reload Flask session to prevent session fixation
    if not CTFd.utils.config.can_send_mail():
        flask.session.regenerate()
    # Open new database session
    with flask.current_app.app_context():
        # Add newly created CTFd account to database
        CTFd.models.db.session.add(account)
        CTFd.models.db.session.commit()
        # Send email
        if CTFd.utils.config.can_send_mail():
            CTFd.utils.email.verify_email_address(account.email)
            infos.append("Check your email to verify your account!")
            CTFd.models.db.session.close()
            return flask.render_template(REGISTER_PAGE, infos=infos, errors=errors)
        # Login CTFd account
        CTFd.utils.security.auth.login_user(account)
        # Go to index page
        CTFd.models.db.session.close()
        return flask.redirect("/")