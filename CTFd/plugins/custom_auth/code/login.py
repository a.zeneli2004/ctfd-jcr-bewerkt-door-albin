import flask
import CTFd

def handle():
    # Temporary values
    infos = CTFd.utils.helpers.get_infos()
    errors = CTFd.utils.helpers.get_errors()
    # Determine login page
    if (CTFd.utils.get_app_config("SETUP_ACCOUNT_TYPE") == "ctfd"):
        LOGIN_PAGE = "login_page_ctfd.html"
        errorString = "You cannot login with an organizational account!"
    if (CTFd.utils.get_app_config("SETUP_ACCOUNT_TYPE") == "surf"):
        LOGIN_PAGE = "login_page_surf.html"
        errorString = "You cannot login with an organizational account!"
    if (CTFd.utils.get_app_config("SETUP_ACCOUNT_TYPE") == "both"):
        LOGIN_PAGE = "login_page_both.html"
        errorString = "It seems this is an organizational account, please login with SURFconext!"
    # Handle GET request
    if flask.request.method == "GET":
        CTFd.models.db.session.close()
        return flask.render_template(LOGIN_PAGE, infos=infos, errors=errors)
    # Retrieve form data
    email = flask.request.form["name"].strip().lower()
    password = flask.request.form["password"].strip()
    # Retrieve CTFd account
    account = CTFd.models.Users.query.filter_by(email=email).first()
    # Check if account exists
    if account:
        # Check if account is from organization
        if account.affiliation != "Unknown" or account.password == None:
            errors.append(errorString)
            CTFd.models.db.session.close()
            return flask.render_template(LOGIN_PAGE, infos=infos, errors=errors)
        # Check if account is not verified
        if not account.verified:
            errors.append("Account must be verified first!")
            CTFd.models.db.session.close()
            return flask.render_template(LOGIN_PAGE, infos=infos, errors=errors)
        # Check if password is correct
        if CTFd.utils.crypto.verify_password(password, account.password):
            # Reload Flask session to prevent session fixation
            flask.session.regenerate()
            # Login CTFd account
            CTFd.utils.security.auth.login_user(account)
            # Go to index page
            CTFd.models.db.session.close()
            return flask.redirect("/")
    # Notify user
    errors.append("Credentials are incorrect!")
    CTFd.models.db.session.close()
    return flask.render_template(LOGIN_PAGE, infos=infos, errors=errors)