import flask
import CTFd

from . import tools

def handle():
    # Temporary values
    SETTINGS_PAGE = "settings_page.html"
    infos = CTFd.utils.helpers.get_infos()
    errors = CTFd.utils.helpers.get_errors()
    account = CTFd.utils.user.get_current_user()
    # Check if not logged in
    if not account:
        return flask.redirect("/")
    # Check if account is from organization
    if account.affiliation != "Unknown":
        errors.append("You cannot change an organizational account!")
        CTFd.models.db.session.close()
        return flask.render_template(SETTINGS_PAGE, infos=infos, errors=errors)
    # Handle GET request
    if flask.request.method == "GET":
        CTFd.models.db.session.close()
        return flask.render_template(SETTINGS_PAGE, infos=infos, errors=errors)
    # Retrieve form data
    password = flask.request.form.get("password", "").strip()
    # Validate form data
    errors.extend(tools.validate_strong_password(password))
    # Check for errors
    if len(errors) > 0:
        CTFd.models.db.session.close()
        return flask.render_template(SETTINGS_PAGE, infos=infos, errors=errors)
    # Change password
    account.password = password
    CTFd.models.db.session.commit()
    # Notify user
    infos.append("Successfully changed password!")
    CTFd.models.db.session.close()
    return flask.render_template(SETTINGS_PAGE, infos=infos, errors=errors)