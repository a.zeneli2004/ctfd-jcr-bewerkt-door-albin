import mysql.connector
import json

mydb = mysql.connector.connect(
    host="127.18.0.2",
    user="ctfd",
    password="ctfd",
    database="ctfd"
)

mycursor = mydb.cursor()

with open('challenges.json') as f:
    data = json.load(f)

    b = data['results']
    for i in range(0, len(b)):
        try:
            id = b[i]['id']
            name = b[i]['name']
            description = b[i]['description']
            max_attempts = b[i]["max_attempts"]
            value = b[i]["value"]
            category = b[i]["category"]
            type = b[i]["type"]
            state = b[i]['state']
            sql = "INSERT INTO challenges (id,name,description,max_attempts,value,category,type,state) VALUES (%s, %s,%s, %s,%s, %s,%s, %s)"
            val = (id, name, description, max_attempts,
                   value, category, type, state)
            mycursor.execute(sql, val)
        except mysql.connector.errors.IntegrityError as e:
            print(e, "while importing challenges")
f.close()

with open('flags.json') as f:
    data = json.load(f)
    b = data['results']
    for i in range(0, len(b)):
        try:
            id = b[i]['id']
            challenge_id = b[i]['challenge_id']
            type_flag = b[i]['type']
            content = b[i]['content']
            sql = "INSERT INTO flags (id,challenge_id,type,content) VALUES (%s,%s,%s,%s)"
            val = (id, challenge_id, type_flag, content)
            mycursor.execute(sql, val)
        except mysql.connector.errors.IntegrityError as e:
            print(e, "while importing flags")
f.close()

mydb.commit()

print("Challenges imported successfully")
